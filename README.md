# README #

These are the files related to Web Tecnology course project. 
The project consist of a web application that allows you to read some scientific papers and create some annotations over them.
These annotations are uploaded to a SPARQL database (no longer active) and shared with the other groups of students.
It's possible to view your own annotation and/or the annotation of other users.
