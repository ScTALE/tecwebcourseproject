/*FILE DI GESTIONE DELLA MODALITA' ANNOTATOR */


/*Cosa serve (ipotizzo)
 - Qualcosa che prenda la selezione;
 - Qualcosa che prenda l'xpath;
 - Qualcosa che data la selezione calcoli gli start ed end;
 - Qualcosa che prenda tutti i dati e crei il json e che lo stampi in console;
 - Qualcosa che invii il json al file che gestisce la conversione in turtle e salvi sta merda in grafo;
 - Qualcosa che gestisca i cambiamenti a livello di struttura dati e a livello di visualizzazione. riga 626 di raschietto js come hint
*/

/*variabili globali utili per la modalita' annotator */
var is_logged = false; //la uso per controllare che i due campi del login siano stati riempiti > false == non loggato, true == 
var testo_html;
var xpath;
var n_getsel;
var user, mail;
//questa è una variabile che contiene l'informazione che ci serve prendere dalla modale
var variabile_modale;
var array_citazione=[];

var tutto_salvato=0;

//funzione per controllare che i dati di login siano stati inseriti

$("#risultato").on('mouseup', function(e){
    ottieni_selezione_e_parametri(e);
});

$(".label_locali").click(function() {
    //console.log(is_logged);
	if(is_logged==false) {
		$(".modifica_ann").hide();
		$(".cancella_ann").hide();
	}
	else {
		$(".modifica_ann").show();
		$(".cancella_ann").show();
	}
});

/*$("body").on("click",".ltw1511",function(e){
    if(is_logged==true) {
        var elem=$(e.currentTarget).attr("id");
        modifica_annotazione(elem);


        e.stopPropagation();
    }
});*/


function azzera_variabili_selezione() {
	testo_html=undefined;
    xpath==undefined;
    n_getsel=undefined;
}

function ottieni_selezione_e_parametri(e) {
    if (is_logged === true){ //controllo del login, per le nostre prove basta commentare l'if
        var testo_selezionato = getSel();
        var alessio = catchSel(testo_selezionato);
        //console.log("sonon qua"+alessio);
        //console.log(alessio[0]);
        //console.log(alessio[1]);
        if(testo_selezionato == ""){ //mi serve per distinguere le selezioni valide da quelle non valide
            console.log("vuoto");
            azzera_variabili_selezione();

        }
        else{
            //console.log(testo_selezionato.getRangeAt(testo_selezionato.rangeCount-1));
            //console.log(testo_selezionato.anchorNode.parentNode);
            //console.log(testo_selezionato.focusNode.parentNode);
            console.log(testo_selezionato);
            var test=valida_selezione(testo_selezionato.getRangeAt(testo_selezionato.rangeCount-1));
            if(test){
                testo_html = 0; //devo riazzerare questa variabile perche' altrimenti succedono cose terribili
                xpath = 0;
                n_getsel = 0;
                var n = {  //qui andrà definito il json
                    node: testo_selezionato.anchorNode.parentNode.id,
                    start: alessio[0],
                    end: alessio[1]
                }
                if(n.start>=n.end) {  //se ho selezionato da destra verso sinistra inverto i campi di start e end
                    var tmp;
                    tmp=n.start;
                    n.start=n.end;
                    n.end=tmp;
                }
                //mi costriuisco altre info come l'xpath
                //console.log("non vuoto======================================="); 
                //console.log(n.node);
                //console.log(e);
                var prova = getXPath(e.target);
                //console.log('xpath ricavato dalla selezione '+prova);
                //var prova_trasformata = creo_selettore_corretto(prova);
                //console.log("xpath modificato: "+prova_trasformata);
                if(n.start>=n.end) {
                	console.log("c'è qualcosa che non va negli start end");
                	azzera_variabili_selezione();
                    alert("Selezione non valida");
                }
                else {
                    //console.log(testo_selezionato.anchorNode.substringData(testo_selezionato.anchorOffset, document.getSelection().toString().length));
                    n_getsel = n;//la porzione evidenziata con il mouse
                    testo_html = testo_selezionato.anchorNode.substringData(testo_selezionato.anchorOffset, document.getSelection().toString().length);
                    xpath = creo_selettore_corretto(prova);//l'xpath della selezione

                    console.log("Dopo: "+testo_html);
                    //console.log("Dopo: "+xpath);
                    console.log("Dopo: "+alessio[0]+','+alessio[1]);
                    //console.log(prova_trasformata);
                }
            }
            
           
            else
                {
                	console.log("anchor node e focus node sono diversi");
                	azzera_variabili_selezione();
                    alert("Selezione non valida");
        	}
            
        }
    }
}






function valida_dati_login(){
    var username = document.getElementById('utente').value;
    var email = document.getElementById('email').value;
    if((username!=undefined)&&(email!=undefined)) {
        if((username.length!=0)&&(email.length!=0)) {
            if(checkEmail()==true) {
                is_logged = true;
                $('#annotamelotuttobella').show();
                 console.log("nome: "+username+" email: "+email);
                 user = username;
                 mail = email;
                return true; 
            }
            else alert("email non valida");
        }
        else alert("username o email con lunghezza nulla");
    }
    else alert("username o email non definito");
    is_logged = false;
    return false;
}
function checkEmail() {
    var email = document.getElementById('email');
    //Questa espressione regolare l'ho trovata su un sito, STI CAZZI
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    if (!filter.test(email.value)) {
        //alert('Please provide a valid email address');
        email.focus;
        return false;
    }
    else{
        return true;
    }
}
function preliminare(code){
    if(code == 1){
        $(".modale_autore").html("");
        //console.log("hai iniziato l'autore");
        var p = $("<p>");
        p.append("Stai annotando: <div id='selectInModal'>"+testo_html+"</div><br>premi Ok per confermare l'annotazione o Close per scartarla<br> Se hai gia' annotato altri autori puoi sceglierli qua:");


        var k;
        var sel=$("<select class='selezione_autore' onblur=variabile_modale=this.value>");
        for(k=0;k<elenco_autori_doc_visualizzato.length;k++) {
            //p.append("<p>"+elenco_autori_doc_visualizzato[k]+"</p>");
            sel.append('<option value="'+elenco_autori_doc_visualizzato[k]+'">'+elenco_autori_doc_visualizzato[k]+'</option>');
        }
        p.append(sel);
        p.append("</select>");
        p.append("oppure inserirlo manualmente: <br>");
        p.append("<input type='text' class='selezione_autore' onblur=variabile_modale=this.value placeholder='autore'></input");

        p.append("</p>");
        $(".modale_autore").append(p);
        
    }
    else
        if(code == 2){
            $(".modale_titolo").html("");
            var p = $("<p>");
            p.append("Stai annotando: <div id='selectInModal'>"+testo_html+"</div><br>premi Ok per confermare l'annotazione o Close per scartarla");
            p.append("</p>");
            $(".modale_titolo").append(p);
            console.log("hai iniziato il titolo");
        }
        else
            if(code == 3){
                $(".modale_doi").html("");
                var p = $("<p>");
                p.append("Stai annotando: <div id='selectInModal'>"+testo_html+"</div><br>premi Ok per confermare l'annotazione o Close per scartarla");
                p.append("</p>");
                $(".modale_doi").append(p);
                console.log("hai iniziato il doi");
            }
            else
                if(code == 4){
                    $(".modale_data").html("");
                    var p = $("<p>");
                    p.append("Stai annotando: <div id='selectInModal'>"+testo_html+"</div><br>premi Ok per confermare l'annotazione o Close per scartarla");
                    p.append("</p>");
                    $(".modale_data").append(p);
                    console.log("hai inizialto l'anno");
                }
                else
                    if(code == 5){
                        $(".modale_url").html("");
                        var p = $("<p>");
                        p.append("Stai annotando: <div id='selectInModal'>"+testo_html+"</div><br>premi Ok per confermare l'annotazione o Close per scartarla");
                        p.append("</p>");
                        $(".modale_url").append(p);
                        console.log("hai iniziato l'url");
                    }
                    else
                        if(code == 6){
                            $(".modale_citazione").html("");
                            var p = $("<p>");
                            p.append("Stai annotando: <div id='selectInModal'>"+testo_html+"</div><br>premi Ok per confermare l'annotazione o Close per scartarla<br>");
                            p.append("<p> Qua sotto inserisci le informazioni inerenti la citazione: </p><br>")
                            p.append("<input type='text'id='autore_cita' placeholder='Autore1'  onblur='array_citazione[0]=this.value'   >   </input><br>");
                            p.append("<input type='text'id='anno_cita' placeholder='Anno di Pubblicazione'  onblur='array_citazione[1]=this.value'    >  </input><br>");

                            p.append("<input type='text'id='autore_cita1' placeholder='Autore2'  onblur='array_citazione[2]=this.value'   >   </input><br>");
                            p.append("<input type='text'id='titolo_cita' placeholder='Titolo della Pubblicazione'  onblur='array_citazione[3]=this.value'    >  </input><br>");


                            p.append("</p>");
                            $(".modale_citazione").append(p);
                            console.log("hai iniziato la citazione");
                        }
                        else
                            if(code == 7){
                                $(".modale_commento").html("");
                                var p = $("<p>");
                                p.append("Stai annotando: <div id='selectInModal'>"+testo_html+"</div><br>premi Ok per confermare l'annotazione o Close per scartarla");
                                p.append("</p>");
                                p.append("Inserici nel campo sottostante il tuo commento<br>");
                                p.append("<input type='text'id='testo_commento' placeholder='Commento' onblur=variabile_modale=this.value> </input>");
                                $(".modale_commento").append(p);
                                console.log("hai iniziato il commento");

                            }
                            else{
                                $(".retMod").html("");
                                var p = $("<p>");
                                p.append("Selezione la funzione di retorica che desideri: <br>");
                                p.append("<select id = 'selezione_di_retorica' onblur=variabile_modale=this.value> \
                                    <option value='Abstract'>Abstract</option> \
                                    <option value='Introduzione'>Introduzione</option> \
                                    <option value='Materiali'>Materiali</option> \
                                    <option value='Metodi'>Metodi</option> \
                                    <option value='Risultati'>Risultati</option> \
                                    <option value='Discussione'>Discussione</option> \
                                    <option value='Conclusione'>Conclusione</option> \
                                </select>");
                                p.append("</p>");
                                $(".retMod").append(p);
                                console.log("hai iniziato la retorica");
                            }

}


//ho trovato questa funzione su stackoverflow per ricavare l'xpath
function getXPath( element )
{
var val=element.value;
    //alert("val="+val);
    var xpath = '';
    for ( ; element && element.nodeType == 1; element = element.parentNode )
    {
        //alert(element);
        var id = $(element.parentNode).children(element.tagName).index(element) + 1;
        id > 1 ? (id = '[' + id + ']') : (id = '');
        xpath = '/' + element.tagName.toLowerCase() + id + xpath;
    }
    return xpath;
}

//scrivo degli scarabocchi di funzione sulla selection guardando MSDN
function getSel() {
			console.log(navigator.appCodeName);
            if (window.getSelection) {
            	console.log("getSel caso 1");
                return window.getSelection();
            } else if (document.getSelection) {
            	console.log("getSel caso 2");
                return document.getSelection();
            } else if (document.selection) {
            	console.log("getSel caso 3");
                return document.selection.createRange().text;
            }
            console.log("getSel caso 4 non gestito");
}

function catchSel(sel){ 
    if(sel){
        var sele = sel;
        var lista = [];
        var targetNode;
        var start, end, offset = 0;
        
        if(sele.rangeCount == 0) return;

        targetNode = commonAncestor(sele.anchorNode, sele.focusNode);
        
        while(targetNode.nodeName=="SPAN" || targetNode.nodeName=="#text"){
            targetNode = targetNode.parentNode;
        }
        
        createList(lista, targetNode);
        
        for(var i=0; i<lista.length; i++){


        	try {
        		if(lista[i].isSameNode(sele.anchorNode)){
                	start = offset + sele.anchorOffset;
            	}
            	if(lista[i].isSameNode(sele.focusNode)){
                	end = offset + sele.focusOffset;
            	}
            	offset += lista[i].textContent.length;
        	}
        	catch(e) {
        		//firefox e browser che non supportano isSameNode
        		if(lista[i].isEqualNode(sele.anchorNode)){
                	start = offset + sele.anchorOffset;
            	}
            	if(lista[i].isEqualNode(sele.focusNode)){
                	end = offset + sele.focusOffset;
            	}
            	offset += lista[i].textContent.length;
        	}
            
        }
        
        if(start>end){
            var t=end;
            end=start;
            start=t;
        }

        return([start, end, targetNode]);
    }
}
function createList(array, element){
    function createList2(array, element, isLast){
        if(element==undefined) return;
  
        var isLast2 = isLast && element==element.parentNode.lastChild;
  
        if(element.firstChild != undefined){
            if(element.firstChild!=element.lastChild || isLast==false) createList2(array, element.firstChild, false);
            else createList2(array, element.firstChild, true);
        }
        else{
            array.push(element);
        }
        
        if(element.nextSibling!=undefined){
            if(element!=element.parentNode.lastChild || isLast==false){
                createList2(array, element.nextSibling, isLast2);
            }
        }
    }
  
    if(element.firstChild != undefined) createList2(array, element.firstChild, true);
    else array[0] = element;
}
function commonAncestor(node1, node2){
    var Alist1 = [];    //lista di tutti gli antenati di node1
    var Alist2 = [];    //lista di tutti gli antenati di node2
    var i = 0;
    var retval;
  
    while(node1.nodeName != "HTML"){    //creazione di Alist1
        Alist1[i] = node1;
        node1 = node1.parentNode;
        i++;
    }

    i=0;
  
    while(node2.nodeName != "HTML"){    //creazione di Alist2
        Alist2[i] = node2;
        node2 = node2.parentNode;
        i++;
    }
  
    var indice1, indice2;
    indice1 = Alist1.length - 1;
    indice2 = Alist2.length - 1;
  
    while( Alist1[indice1]==Alist2[indice2] && indice1!=-1 && indice2!=-1 ){   //ricerca antenato comune
        retval = Alist1[indice1];
        indice1--;
        indice2--;
    }
  
    return retval;
}

function creo_selettore_corretto(xpath){
    //console.log('sto correggendo il selettore');
    //mi creo alcune variabili per definire, per ogni dominio, il selettore corretto da inizializzare
    var corretto = ''; //la variabile contenente il selettore corretto da sparare nel json
    var dlib = 'form[1]/table[3]/tr/td/table[5]/tr/td/table[1]/tr/td[2]';
    var rivista_statistica = 'div/div[2]/div[2]/div[3]';
    var almatourism = 'div/div[2]/div[2]/div[3]';
    var antropologia_e_teatro = 'div/div[2]/div[2]/div[3]';

    //ora cerco nella variabile url_doc_visual il dominio
    if (url_doc_visual.indexOf('dlib') > -1){
        corretto = corretto + dlib;
    }
    else{
        if(url_doc_visual.indexOf('rivista-statistica.unibo.it') > -1){
            corretto = corretto + rivista_statistica;
        }
        else{
            if (url_doc_visual.indexOf('almatourism.unibo.it') > -1){
                corretto = corretto + almatourism;
            }
            else{
                if (url_doc_visual.indexOf('antropologiaeteatro.unibo.it') > -1){
                    corretto = corretto + antropologia_e_teatro;
                }
                else
                    return corretto; //se arrivo a questo caso vuol dire che il documento non e' tra quelli da filtrare pertanto il selettore e' gia' quello giusto
            }
        }
    }

    //rimuovo tutti gli eventuali span dentro a xpath
    while(xpath.indexOf("span")>-1) {
        if(xpath.charAt(xpath.indexOf("span")+4)=="[") {
            var indice=xpath.indexOf("span")+4,conta=0;
            while(xpath.charAt(indice)!="]") {
                conta++;
                indice++;
            }
            //4 caratteri per la parola span
            //inizio da -1 perchè prendo anche lo /
            //+1 perchè parto da -1
            xpath=xpath.replace(xpath.substring(xpath.indexOf("span")-1,xpath.indexOf("span")+4+conta+1),"");   
        }
        else xpath = xpath.replace('/span', '');
    }
    //ora rimuovo /html/body che assolutamente non va mai inserito altrimenti ci sclerano dietro
    xpath = xpath.replace('/html/body','');

    //elimino il percorso fino al div risultato
    //xpath=xpath.replace('div[2]/div/div[2]/div/div[2]/div/div/div/','');
    xpath=xpath.replace('div/div/div[2]/div/div/div/div/div[2]/','');                                

    
    var splitter=xpath.split("/");
    var k,agg="";
    for(k=0;k<splitter.length;k++) {
        if(splitter[k].length>0) {
            if(splitter[k].charAt(splitter[k].length-1)!="]") {
                splitter[k]=splitter[k]+"[1]";
            }
            splitter[k]="/"+splitter[k];
        }
        agg=agg+splitter[k];
    }
    xpath=agg;

    //ora compongo il selettore finale
    corretto = corretto + xpath;
    return corretto; //do you wanna a correct coffee?
}
function crea_json(code,frutto){ //caso base di json visto che alcuni hanno delle variazioni
	//da mofidicare la gestione del frutto
    var selection = n_getsel;
    var posizione_nel_doc = xpath;  
    if(code == 1){ //autore
        if(frutto=="arancia") {
            $("div#autore").modal('toggle');
        }
        var preliminari = {
                "type": "",
                "label": "",
                "body": {
                    "label": "",
                    "subject": "",
                    "predicate": "",
                    "resource": {
                        "id":"",
                        "label": "",
                    }
                },
                "target": {
                    "source": "",
                    "id": "",
                    "start": "",
                    "end": ""
                },
                "provenance": {
                    "author": {
                        "name": "",
                        "email": "",
                    },
                    "time":""
                },
               	"salvata": 1,
            };
        preliminari.type = 'hasAuthor';
        preliminari.label = 'Autore';
        preliminari.body.label = 'Un autore del documento e '+ rimuovi_apici_turtle(testo_html);
        preliminari.body.subject = crea_subject_corretto(url_doc_visual);//da sistemare
        preliminari.body.predicate = 'dcterms:creator';
        preliminari.body.resource.id = check_iri_autore(crea_uri_autore(testo_html));
        
        if (frutto == 'arancia') {

            var aut;
            aut = variabile_modale;
            if(aut==undefined) preliminari.body.resource.label = rimuovi_apici_turtle(testo_html);
            else {
                preliminari.body.resource.label = rimuovi_apici_turtle(aut);
            }
            variabile_modale=undefined;

            
        }
        else preliminari.body.resource.label = $("#nuovo_autore").val();
        
        preliminari.provenance.author.email = mail;
        preliminari.provenance.author.name = user;
        preliminari.provenance.time = theDate();
        preliminari.target.source = url_doc_visual;
        preliminari.target.id = posizione_nel_doc;
        preliminari.target.start = selection.start;
        preliminari.target.end = selection.end;
        //console.log("hai salvato l'autore");

        lista_json[ultimo].annotations.push(preliminari);
    	var per_visualizzare={
    		annotations:[],
            "provenance": {
                "author": {
                  "name": user,
                    "email": mail
                },
                "time": preliminari.provenance.time
            }
   	 	};
    	per_visualizzare.annotations.push(preliminari);

        var id_bottoni=["#mostra1"];
        var tipi=["hasAuthor"];
        var classi=["autore"];

    	visualizza_annotazioni(per_visualizzare,id_bottoni,tipi,classi,"ltw1511",1);

    }
    else
        if(code == 2){ //titolo
            if(frutto=="arancia") {
                $("div#titolo").modal('toggle');
            }
            var preliminari = {
                    "type": "",
                    "label": "",
                    "body": {
                        "subject": "",
                        "predicate": "",
                        "object": "",
                    },
                    "target": {
                        "source": "",
                        "id": "",
                        "start": "",
                        "end": ""
                    },
                    "provenance": {
                        "author": {
                            "name": "",
                            "email": "",
                        },
                        "time":""
                    },
                    "salvata": 1,
                };
            preliminari.type = 'hasTitle';
            preliminari.label = 'Titolo';

            if(frutto=="arancia") preliminari.body.object = rimuovi_apici_turtle(testo_html);
            else preliminari.body.object=$("#nuovo_tipo").val();

            preliminari.body.subject = crea_subject_corretto(url_doc_visual);//da sistemare
            preliminari.body.predicate = 'dcterms:title';
            preliminari.provenance.author.email = mail;
            preliminari.provenance.author.name = user;
            preliminari.provenance.time = theDate();
            preliminari.target.source = url_doc_visual;
            preliminari.target.id = posizione_nel_doc;
            preliminari.target.start = selection.start;
            preliminari.target.end = selection.end; 
            //console.log("hai salvato l'autore");

            lista_json[ultimo].annotations.push(preliminari);
            var per_visualizzare={
                annotations:[],
                "provenance": {
	                "author": {
	                  "name": user,
	                    "email": mail
	                },
	                "time": preliminari.provenance.time
	            }
            };
            per_visualizzare.annotations.push(preliminari);

           	var id_bottoni=["#mostra4"];
			var tipi=["hasTitle"];
			var classi=["titolo"];

            visualizza_annotazioni(per_visualizzare,id_bottoni,tipi,classi,"ltw1511",1);
            //console.log("hai salvato il titolo");
        }
        else
            if(code == 3){
                if(frutto=="arancia") {
                    $("div#doi").modal('toggle');
                }
                //$("div.modal-body").hide();
                var preliminari = {
                        "type": "",
                        "label": "",
                        "body": {
                            "subject": "",
                            "predicate": "",
                            "object": "",
                        },
                        "target": {
                            "source": "",
                            "id": "",
                            "start": "",
                            "end": ""
                        },
                        "provenance": {
                            "author": {
                                "name": "",
                                "email": "",
                            },
                            "time":""
                        },
                        "salvata": 1,
                    };
                preliminari.type = 'hasDOI';
                preliminari.label = 'DOI';
                
                if (frutto == 'arancia') preliminari.body.object = rimuovi_apici_turtle(testo_html);
                else preliminari.body.object = $("#nuovo_tipo").val();

                preliminari.body.subject = crea_subject_corretto(url_doc_visual);//da sistemare
                preliminari.body.predicate = 'prism:doi';
                preliminari.provenance.author.email = mail;
                preliminari.provenance.author.name = user;
                preliminari.provenance.time = theDate();
                preliminari.target.source = url_doc_visual;
                preliminari.target.id = posizione_nel_doc;
                preliminari.target.start = selection.start;
                preliminari.target.end = selection.end;
                //console.log("hai salvato l'autore");

                lista_json[ultimo].annotations.push(preliminari);
                var per_visualizzare={
                    annotations:[],
	                "provenance": {
		                "author": {
		                  "name": user,
		                    "email": mail
		                },
		                "time": preliminari.provenance.time
		            }
                };
                per_visualizzare.annotations.push(preliminari);

                var id_bottoni=["#mostra3"];
				var tipi=["hasDOI"];
				var classi=["DOI"];

                visualizza_annotazioni(per_visualizzare,id_bottoni,tipi,classi,"ltw1511",1);
                //console.log("hai salvato il doi");
            }
            else
                if(code == 4){
                    if(frutto=="arancia") {
                        $("div#publ").modal('toggle');
                    }
                    //$("div.modal-body").hide();
                    var preliminari = {
                            "type": "",
                            "label": "",
                            "body": {
                                "subject": "",
                                "predicate": "",
                                "object": "",
                            },
                            "target": {
                                "source": "",
                                "id": "",
                                "start": "",
                                "end": ""
                            },
                            "provenance": {
                                "author": {
                                    "name": "",
                                    "email": "",
                                },
                                "time":""
                            },
                            "salvata": 1,
                        };
                    preliminari.type = 'hasPublicationYear';
                    preliminari.label = 'Anno di Pubblicazione';
                    
                    if (frutto == 'arancia') preliminari.body.object = rimuovi_apici_turtle(testo_html);
                    else preliminari.body.object = $('#nuovo_tipo').val();
                    
                    preliminari.body.subject = crea_subject_corretto(url_doc_visual);//da sistemare
                    preliminari.body.predicate = 'fabio:hasPublicationYear';
                    preliminari.provenance.author.email = mail;
                    preliminari.provenance.author.name = user;
                    preliminari.provenance.time = theDate();
                    preliminari.target.source = url_doc_visual;
                    preliminari.target.id = posizione_nel_doc;
                    preliminari.target.start = selection.start;
                    preliminari.target.end = selection.end;
                    //console.log("hai salvato l'autore");

                    lista_json[ultimo].annotations.push(preliminari);
                    var per_visualizzare={
                        annotations:[],
                        "provenance": {
			                "author": {
			                  "name": user,
			                    "email": mail
			                },
			                "time": preliminari.provenance.time
			            }
                    };
                    per_visualizzare.annotations.push(preliminari);

                    var id_bottoni=["#mostra2"];
					var tipi=["hasPublicationYear"];
					var classi=["data"];

                    visualizza_annotazioni(per_visualizzare,id_bottoni,tipi,classi,"ltw1511",1);
                    //console.log("hai salvato l'anno");
                }
                else
                    if(code == 5){
                        if(frutto=="arancia") {
                            $("div#url").modal('toggle');
                        }
                        //$("div.modal-body").hide();
                        var preliminari = {
                                "type": "",
                                "label": "",
                                "body": {
                                    "subject": "",
                                    "predicate": "",
                                    "object": "",
                                },
                                "target": {
                                    "source": "",
                                    "id": "",
                                    "start": "",
                                    "end": ""
                                },
                                "provenance": {
                                    "author": {
                                        "name": "",
                                        "email": "",
                                    },
                                    "time":""
                                },
                                "salvata": 1,
                            };
                        preliminari.type = 'hasURL';
                        preliminari.label = 'URL';
                        
                        /*if (frutto == 'arancia') preliminari.body.object = testo_html;
                        else preliminari.body.object = $("#nuovo_tipo").val();*/

                        preliminari.body.object = rimuovi_apici_turtle(testo_html);
                        preliminari.body.subject = crea_subject_corretto(url_doc_visual);//da sistemare
                        preliminari.body.predicate = 'fabio:hasURL';
                        preliminari.provenance.author.email = mail;
                        preliminari.provenance.author.name = user;
                        preliminari.provenance.time = theDate();
                        preliminari.target.source = url_doc_visual;
                        preliminari.target.id = posizione_nel_doc;
                        preliminari.target.start = selection.start;
                        preliminari.target.end = selection.end;
                        //console.log("hai salvato l'autore");

                        lista_json[ultimo].annotations.push(preliminari);
                        var per_visualizzare={
                            annotations:[],
                            "provenance": {
                                "author": {
                                  "name": user,
                                    "email": mail
                                },
                                "time": preliminari.provenance.time
                            }
                        };
                        per_visualizzare.annotations.push(preliminari);

                        var id_bottoni=["#mostra8"];
                        var tipi=["hasURL"];
                        var classi=["url"];

                        visualizza_annotazioni(per_visualizzare,id_bottoni,tipi,classi,"ltw1511",1);
                        //console.log("hai salvato il doi");
                    }
                    else
                        if(code == 6){
                        	//console.log(array_citazione);
                            if(frutto=="arancia") {
                                $("div#cites").modal('toggle');
                            }
                            var preliminari = {
                                "type": "",
                                "label": "",
                                "body": {
                                    "subject": "", //nomedominio:nomepaginahtml
                                    "predicate": "", //cito:cites
                                    "resource": {
                                    	"id": "", //nomedominio:cited_
                                    	"label": "", //testo selezionato
                                    	annotations: [],					
                                    },
                                },
                                "target": {
                                    "source": "",
                                    "id": "",
                                    "start": "",
                                    "end": ""
                                },
                                "provenance": {
                                    "author": {
                                        "name": "",
                                        "email": "",
                                    },
                                    "time":""
                                },
                                "salvata": 1,
                            };

                            preliminari.type="cites";
                            preliminari.label="Questo documento cita: " + rimuovi_apici_turtle(testo_html);
                            preliminari.body.subject=crea_subject_corretto(url_doc_visual);
                            preliminari.body.predicate="cito:cites";

                            preliminari.body.resource.id=crea_subject_corretto(url_doc_visual)+"_cited";	//da fare
                            preliminari.body.resource.label=rimuovi_apici_turtle(testo_html);

                            preliminari.provenance.author.email = mail;
                            preliminari.provenance.author.name = user;
                            preliminari.provenance.time = theDate();
                            preliminari.target.source = url_doc_visual;
                            preliminari.target.id = posizione_nel_doc;
                            preliminari.target.start = selection.start;
                            preliminari.target.end = selection.end;

                            var scorro;
                            //il 2 va cambiato a seconda di quanti campi prendiamo dal widget della citazione
                            for(scorro=0;scorro<4;scorro++) {
                            	var j_push={
									"type": "",               	//
									"label": "",				//
							        "body": {
							            "subject": '',
							            "predicate": '',
							            "object": "oggetto",			//
                                        "resource": {
                                           "id": "http://vitali.web.cs.unibo.it/raschietto/person/citation",
                                           "label": "label_citazione_autore"
                                        }
							        },
							        "target": {
							            "source": '',
							            "id": "",
								        "start": "",
							            "end": ""
							        }
								};

                                j_push.body.subject = preliminari.body.subject+"_cited";
                                j_push.body.predicate = preliminari.body.predicate;
                                j_push.body.object = preliminari.body.label; 
                                j_push.target.source = url_doc_visual;
                                j_push.target.id = posizione_nel_doc;
                                j_push.target.start = selection.start;
                                j_push.target.end = selection.end;

								if(scorro==0 || scorro==2) {
                                     //cosa aggiuntiva
                                    j_push.body.predicate = "dcterms:creator";

                            		j_push.type="hasAuthor";
                                    if(frutto=="arancia") {
                                		if(array_citazione[scorro]==undefined) array_citazione[scorro]="Autore"; 
                                		j_push.label=array_citazione[scorro];
                                    }
                                    else {
                                        if($("#nuovo_autore_cit1").val()==undefined) j_push.label="Autore";
                                        else {
                                            if(scorro==0) j_push.label=$("#nuovo_autore_cit1").val();
                                            else j_push.label=$("#nuovo_autore_cit2").val();
                                        }
                                    }
                            		array_citazione[scorro]=undefined;
                            	}
                           	 	if(scorro==1) {
                                    //cosa aggiuntiva
                                    j_push.body.predicate = "fabio:hasPublicationYear";

                            		j_push.type="hasPublicationYear";
                                    if(frutto == 'arancia'){
                                        if(array_citazione[scorro]==undefined) array_citazione[scorro]="Data";
                                        j_push.label=array_citazione[scorro];    
                                    }
                            		else {
                                        if($("#nuovo_anno").val() == undefined) j_push.label = "Data";
                                        else j_push.label = $("#nuovo_anno").val();
                                    }
                            		array_citazione[scorro]=undefined;
                           		}
                                if(scorro == 3){
                                    // cosa aggiuntiva
                                    j_push.body.predicate = "dcterms:hasTitle";

                                    j_push.type = 'hasTitle';
                                    if(frutto == 'arancia'){
                                        if(array_citazione[scorro] == undefined) array_citazione[scorro] = 'Title';
                                        j_push.label = array_citazione[scorro];
                                    }
                                    else{
                                        if($("#nuovo_titolo").val() == undefined) j_push.label = 'Title';
                                        else j_push.label = $('#nuovo_titolo').val();

                                    }
                                }
                            	preliminari.body.resource.annotations.push(j_push);
                            }
                            


                            lista_json[ultimo].annotations.push(preliminari);
	                        var per_visualizzare={
	                            annotations:[],
	                            "provenance": {
					                "author": {
					                  "name": user,
					                    "email": mail
					                },
					                "time": preliminari.provenance.time
					            }
	                        };
	                        per_visualizzare.annotations.push(preliminari);

                            var id_bottoni=["#mostra7"];
							var tipi=["cites"];
							var classi=["citazione"];
							visualizza_annotazioni(per_visualizzare,id_bottoni,tipi,classi,"ltw1511",1);

                            var json_steve={
                                    annotations: [],
                                    "provenance": {
                                        "author": {
                                            "name": "",
                                            "email": ""
                                        },
                                        "time": ""
                                    }
                                };


                        var l;
                        for(l=0;l<per_visualizzare.annotations[0].body.resource.annotations.length;l++) {
                            json_steve.annotations.push(per_visualizzare.annotations[0].body.resource.annotations[l]);
                        }
                        json_steve.provenance.author.name = per_visualizzare.provenance.author.name;
                        json_steve.provenance.author.email = per_visualizzare.provenance.author.email;
                        json_steve.provenance.time = per_visualizzare.provenance.time;

                        if(json_steve.annotations.length>0) {
                            var g;
                            for(g=0;g<json_steve.annotations.length;g++) {
                                json_steve.annotations[g].salvata=1;
                            }
                            var id_bottoni=["#mostra1","#mostra2","#mostra3","#mostra4","#mostra5","#mostra6","#mostra7","#mostra8"];
                            var tipi=["hasAuthor","hasPublicationYear","hasDOI","hasTitle","hasComment","denotesRhetoric","cites","hasURL"];
                            var classi=["autore","data","DOI","titolo","commento","retorica","citazione","url"];
                            visualizza_annotazioni(json_steve,id_bottoni,tipi,classi,"ltw1511");
                            aggiusta_citazione();
                            var h;
                            for(h=0;h<json_steve.annotations.length;h++) {
                                lista_json[ultimo].annotations.push(json_steve.annotations[h]);
                            }
                            //console.log("Sottocampi citazioni ", JSON.stringify(json_steve));
                        }
                        else console.log("Sottocampi per citazione non presenti");




                            //console.log("hai salvato la citazione");
                        }
                        else
                            if(code == 7){
                                //alert(variabile_modale);
                                if(frutto=="arancia") {
                                    $("div#comment").modal('toggle');
                                }

                                 var preliminari = {
                                    "type": "",
                                    "label": "",
                                    "body": {
                                        "subject": "",
                                        "predicate": "",
                                        "object": "",
                                    },
                                    "target": {
                                        "source": "",
                                        "id": "",
                                        "start": "",
                                        "end": ""
                                    },
                                    "provenance": {
                                        "author": {
                                            "name": "",
                                            "email": "",
                                        },
                                        "time":""
                                    },
                                    "salvata": 1,
                                };
                                preliminari.type = 'hasComment';
                                var commento;
                                if(frutto=="arancia") {
                                	commento = variabile_modale;
                                	if(commento==undefined) commento="Questo è un commento";
                                }
                                else {
                                	commento=$("#nuovo_tipo").val();
                                }
                                variabile_modale=undefined;
                                preliminari.label = 'Il commento di questo frammento è:' + rimuovi_apici_turtle(commento);
                                preliminari.body.object = rimuovi_apici_turtle(commento); //qua ci vuole il pezzo di retorica che l'utente ha scelto
                                preliminari.body.subject = crea_subject_corretto(url_doc_visual);//da sistemare
                                preliminari.body.predicate = 'schema:comment'; //da modificare perche' al momento attuale non so cosa ci va di preciso
                                preliminari.provenance.author.email = mail;
                                preliminari.provenance.author.name = user;
                                preliminari.provenance.time = theDate();
                                preliminari.target.source = url_doc_visual;
                                preliminari.target.id = posizione_nel_doc;
                                preliminari.target.start = selection.start;
                                preliminari.target.end = selection.end;

                                lista_json[ultimo].annotations.push(preliminari);
                                var per_visualizzare={
                                    annotations:[],
                                    "provenance": {
                                        "author": {
                                          "name": user,
                                            "email": mail
                                        },
                                        "time": preliminari.provenance.time
                                    }
                                };
                                per_visualizzare.annotations.push(preliminari);


                                var id_bottoni=["#mostra5"];
								var tipi=["hasComment"];
								var classi=["commento"];
								visualizza_annotazioni(per_visualizzare,id_bottoni,tipi,classi,"ltw1511",1);
                                //console.log("hai salvato il commento");
                            }
                            else{
                                //8
                                if(frutto=="arancia") {
                                    $("div#retorica").modal('toggle');
                                }
                                var preliminari = {
                                    "type": "",
                                    "label": "",
                                    "body": {
                                        "subject": "",
                                        "predicate": "",
                                        "object": "",
                                    },
                                    "target": {
                                        "source": "",
                                        "id": "",
                                        "start": "",
                                        "end": ""
                                    },
                                    "provenance": {
                                        "author": {
                                            "name": "",
                                            "email": "",
                                        },
                                        "time":""
                                    },
                                    "salvata": 1,
                                };
                                preliminari.type = 'denotesRhetoric';
                                var scelta;
                                if(frutto=="arancia") {
                                	scelta = variabile_modale;
                                	if(scelta==undefined) scelta="Abstract";
                                }
                                else {
                                	scelta=$("#nuovo_tipo").val();
                                }
                                variabile_modale=undefined;
                                preliminari.label = 'Funzione retorica di questo frammento:' + rimuovi_apici_turtle(scelta);
                                preliminari.body.object = "deo:" + rimuovi_apici_turtle(scelta); //qua ci vuole il pezzo di retorica che l'utente ha scelto
                                preliminari.body.subject = crea_subject_corretto(url_doc_visual);//da sistemare
                                preliminari.body.predicate = 'sem:denotes';
                                preliminari.provenance.author.email = mail;
                                preliminari.provenance.author.name = user;
                                preliminari.provenance.time = theDate();
                                preliminari.target.source = url_doc_visual;
                                preliminari.target.id = posizione_nel_doc;
                                preliminari.target.start = selection.start;
                                preliminari.target.end = selection.end;

                                lista_json[ultimo].annotations.push(preliminari);
                                var per_visualizzare={
                                    annotations:[],
                                    "provenance": {
                                        "author": {
                                          "name": user,
                                            "email": mail
                                        },
                                        "time": preliminari.provenance.time
                                    }
                                };
                                per_visualizzare.annotations.push(preliminari);
                                       
                                var id_bottoni=["#mostra6"];
								var tipi=["denotesRhetoric"];
								var classi=["retorica"];
								visualizza_annotazioni(per_visualizzare,id_bottoni,tipi,classi,"ltw1511",1);

                                //console.log("hai salvato la retorica");
                            }
    console.log(JSON.stringify(preliminari));
    //visualizza_nostre_label();
    tutto_salvato=1;
} //qua si chiude la crea json

function theDate(){ //server pre interrogare javascript sull'orario
    var d = new Date();
    //var n = d.getTime();
 	var d=String(d);
 	d=d.split(" ");
 	d=d[2]+"-"+d[1]+"-"+d[3]+" alle "+d[4];
  
    return d;
}
function crea_uri_autore(stringa_autore){
    //questa funzione si chiama solo ed esclusivamente nel caso dell'annotazione sull'autore
    var obbligatorio_per_specifiche = 'http://vitali.web.cs.unibo.it/raschietto/person/';
    var res = stringa_autore.toLowerCase(); //setto tutti i caratteri bassi
    var split = res.split(" "); //divido la stringa in tante parti quanti sono gli spazi
    var second = split[split.length - 1]; //mi cucco il cognome e ora magia
    
    var clean = cleanCode(res);
    var first = clean.slice(0,1); //mi cucco il primo carattere
    
    
    var all = obbligatorio_per_specifiche + first + '-' + second; //autore come da specifica
    return all;
}
function cleanCode (c) { //mi serve per rimuovere tutti i caratteri di punteggiatura
  return c.replace(/[^A-Za-z0-9_]/g,"");
}
/*questa funzione serve a trasformare l'url_doc_visual nel subject corretto per il turtle */
function crea_subject_corretto(stringa_subject){ //questa funzione si chiama sempre per ogni subject
    if (stringa_subject.indexOf('dlib') > -1){
        var res = stringa_subject.toLowerCase();
        var corretto = 'dlib:';
        var split = res.split('/');
        var pagina = split[split.length - 1];
        var second = pagina.replace(".html", "");
        return corretto + second;

    }
    else{
        if(stringa_subject.indexOf('rivista-statistica.unibo.it') > -1){
            var res = stringa_subject.toLowerCase();
            var corretto = 'statistica:';
            var split = res.split('/');
            var pagina = split[split.length - 1];
            var second = pagina.replace(".html", "");
            return corretto + second;
        }
        else{
            if (stringa_subject.indexOf('almatourism.unibo.it') > -1){
                var res = stringa_subject.toLowerCase();
                var corretto = 'almatourism:';
                var split = res.split('/');
                var pagina = split[split.length - 1];
                var second = pagina.replace(".html", "");
                return corretto + second;

            }
            else{
                if (stringa_subject.indexOf('antropologiaeteatro.unibo.it') > -1){
                    var res = stringa_subject.toLowerCase();
                    var corretto = 'antropologiaeteatro:';
                    var split = res.split('/');
                    var pagina = split[split.length - 1];
                    var second = pagina.replace(".html", "");
                    return corretto + second;
                }
                else{
                    var corretto = 'generic_uri';
                    return corretto;
                }
            }
        }
    }
}

//locale è un parametro booleano che dovrebbe determinare se l'annotazione è solo locale o è anche sullo sparql. Per ora non lo uso
function cancella_annotazione(elemento) {
	//cancellazione grafica
	var id="label"+elemento;
	$("#"+id).removeClass();
	$("#"+id).removeAttr("id");
	
	//cancellazione locale
	var k;
	for(k=0;k<lista_json[ultimo].annotations.length;k++) {
		if(lista_json[ultimo].annotations[k].identificatore==id) {
			//questa istruzione non rimuove l'elemento, ma lo tramuta in null
			//questa cosa da fastidio, quindi faccio questo assegnamento brutto ma funzionale 
			//delete lista_json[ultimo].annotations[k];
			lista_json[ultimo].annotations[k]="invalid";
		}
	}
	//console.log(JSON.stringify(lista_json[ultimo].annotations));

	//rimozione minicard di destra
	//console.log($(".num"+elemento).closest("ul"));
	$(".num"+elemento).closest("ul").remove();
    


	//cancellazione dizionario label
	var j;
	for(j=0;j<label_diz.length;j++) {
		if(label_diz[j].key==id) {
			label_diz[j]="invalid";
		}
	}
    //cancellazione remota
    //cancellazione_remota();

}

function modifica_annotazione(elemento) {
	//console.log(elemento);
	//0 non valida, 1 valida
    var selezione_valida=0,tipo;
    //apetura modale bianca per provare 

    $("#scelta_modifica_ann").modal(); 
    $("#scelta_modifica").on("change",function(e) {
    	tipo=$("#scelta_modifica").val();
    	$("#scelta_modifica_ann .modal-body input").remove();
    	if(tipo=="Autore") {
    		//autocompletamento autore da inserire
    		$("#scelta_modifica_ann .modal-body select").after("<input type='text' placeholder='Nuovo dato' id='nuovo_autore'></input>");
    	} 
    	else {
    		if((tipo=="DOI")||(tipo=="Data")||(tipo=="Titolo")||(tipo=="Commento")||(tipo=="Retorica")||(tipo=="Url")) {
    			$("#scelta_modifica_ann .modal-body select").after("<input type='text' placeholder='Nuovo dato' id='nuovo_tipo'></input>");
    		}
    		else {
    			if(tipo=="Citazione") {
    				var trabbata="<input type='text' placeholder='Nuovo Titolo' id='nuovo_titolo'></input>\
    					<input type='text' placeholder='Nuovo anno' id='nuovo_anno'></input>\
    					<input type='text' placeholder='Nuovo autore1' id='nuovo_autore_cit1'></input>\
    					<input type='text' placeholder='Nuovo autore2' id='nuovo_autore_cit2'></input>\
    				";
    				$("#scelta_modifica_ann .modal-body select").after(trabbata);
    			}
    		}
    	}
    });
	if((testo_html!=undefined)&&(xpath!=undefined)&&(n_getsel!=undefined)) {
		$("#fanculo_le_checkbox").removeClass("btn-danger").addClass("btn-success");
		selezione_valida=1;
		$("#fanculo_le_checkbox").text("Hai selezionato una nuova porzione di testo");
	}
	else {
		$("#fanculo_le_checkbox").removeClass("btn-success").addClass("btn-danger");
		selezione_valida=0;
		$("#fanculo_le_checkbox").text("Non hai selezionato una nuova porzione valida di testo");
	}
	//non hai selezionato niente
  	$("#andiamo").click(function(){
  		 	if(selezione_valida==0) {
   		       //abbiamo pigiato modifica senza selezionare testo allora andiamo nel json della minicard e assegnamo alle variabili globali importanti che altrimenti sarebbero vuote valori essenziali 
   		
		   		var id="label"+elemento;
				$("#"+id).removeClass();
				$("#"+id).removeAttr("id");
				var k;
				for(k=0;k<lista_json[ultimo].annotations.length;k++) {
					if(lista_json[ultimo].annotations[k].identificatore==id) {
						
						var vecchi_valori=riempi_a_mano(lista_json[ultimo].annotations[k]);
						//console.log(vecchi_valori);
						xpath=vecchi_valori[0];
						n_getsel={
							node: "nodo",
							start: vecchi_valori[1],
							end: vecchi_valori[2]
						};
						
						testo_html=vecchi_valori[3];
                        cancella_annotazione(elemento);
						if(tipo=="Autore") {
                            crea_json(1,"pesca");
						}
						else if(tipo=="Titolo") {
							crea_json(2,"pesca");
						}
						else if(tipo=="DOI") {
                            crea_json(3,"pesca");
						}
                        else if(tipo == "Data"){
                            crea_json(4,"pesca");
                        }
                        else if(tipo == "Url"){
                            crea_json(5,"pesca");
                        }
                        else if(tipo == "Citazione"){
                            crea_json(6,"pesca");
                        }
                        else if(tipo == "Commento"){
                            crea_json(7,"pesca");
                        }
                        else if(tipo == "Retorica"){

                            crea_json(8,"pesca");
                        }
					}
				}
		   	}
		   	else {
                //annotazione da nova selezione
                //rimozione vecchio span
                //rimozione in lista_json
                //rimozione menù label
                //rimozione in label_diz
                cancella_annotazione(elemento);
                //creazione sfruttando nuovi parametri in memoria
                if(tipo=="Autore") {
                    crea_json(1,"pesca");
                }
                else if(tipo=="Titolo") {
                    crea_json(2,"pesca");
                }
                else if(tipo=="DOI") {
                    crea_json(3,"pesca");
                }
                else if(tipo == "Data"){
                    crea_json(4,"pesca");
                }
                else if(tipo == "Url"){
                    crea_json(5,"pesca");
                }
                else if(tipo == "Citazione"){
                    crea_json(6,"pesca");
                }
                else if(tipo == "Commento"){
                    crea_json(7,"pesca");
                }
                else if(tipo == "Retorica"){

                    crea_json(8,"pesca");
                }
            }
            /*cancella_tutto();
            salva_tutto();*/
  	});
    
}

function riempi_a_mano(json) {
	if((json.type=="hasAuthor")||(json.type=="cites")) {
        return([json.target.id,json.target.start,json.target.end,json.body.resource.label]);
	}
	else {
		return([json.target.id,json.target.start,json.target.end,json.body.object]);
	}
}

function valida_selezione(range){
    var range_start=$(range.startContainer).parent()[0];
    var range_end=$(range.endContainer).parent()[0];
    var parent_range_start_filter=$(range.startContainer).parents().filter(':not(span)').filter(':not(span)')[0];
    var parent_range_end_filter=$(range.endContainer).parents().filter(':not(span)').filter(':not(span)')[0];
    //se start e end non coincidono
    if(!range.collapsed){          
        //controllo il padre
        if(range_start==range_end){
            return true;
        }
        else{
        	//controllo che uno dei due nodi contenga l'altro per intero
        	if($.contains(range_start,range_end) && range.endOffset==$(range.endContainer).text().length){
            	return true;
        	}
        	else {
        		if($.contains(range_end,range_start) && range.startOffset==0){
            		return true;
        		}
        		//o che  i due nodi siano due fratelli completamente selezionati
        		else {
        			if((parent_range_start_filter==parent_range_end_filter || $(parent_range_start_filter).parent()[0]==$(parent_range_end_filter).parent()[0]) && range.startOffset==0 && range.endOffset==$(range_end).text().length){
            			return true;
        			}
        		}
        	}
        }
    }
    return false;
}
function rimuovi_apici_turtle(stringa){
    if(stringa==undefined) {
        return("contenuto non presente");
    }
    else{
        while(stringa.indexOf("\"")> -1) {
            stringa = stringa.replace("\"", "");
        }
        while(stringa.indexOf("\'")> -1) {
            stringa = stringa.replace("\'", "");   
        }
        while(stringa.indexOf("<")> -1) {
            stringa = stringa.replace("<", "");
        }
        while(stringa.indexOf(">")> -1) {
            stringa = stringa.replace(">", "");
        }
        while (stringa.indexOf("/")> -1) {
            stringa = stringa.replace("/", "");
        }
        while (stringa.indexOf("\\")> -1) {
            stringa = stringa.replace("\\", "");
        }
        while (stringa.indexOf("<i>")> -1) {
            stringa = stringa.replace("<i>", "");
        }
        while (stringa.indexOf("</i>")> -1) {
            stringa = stringa.replace("</i>", "");
        }
        while (stringa.indexOf(".")> -1) {
            stringa = stringa.replace(".", " ");
        }
        while (stringa.indexOf(",")> -1) {
            stringa = stringa.replace(",", " ");
        }
        while (stringa.indexOf("<a>")> -1) {
            stringa = stringa.replace("<a>", "");
        }
        while (stringa.indexOf("<b>")> -1) {
            stringa = stringa.replace("<b>", "");
        }
        while (stringa.indexOf("</a>")> -1) {
            stringa = stringa.replace("</a>", "");
        }
        while (stringa.indexOf("</b>")> -1) {
            stringa = stringa.replace("</b>", "");
        }
        return(stringa);
    }
}
function check_iri_autore(stringa){
    if(stringa == undefined){
        return ("http://vitali.web.cs.unibo.it/raschietto/person/n-nan");
    }
    else{
        while(stringa.indexOf(' ')> -1){
            stringa = stringa.replace(' ', "a");
        }
        return (stringa);
    }
}

function cancellazione_remota() {
    $.ajax({
        url: "core/lib/annotation_manager/svuota_grafo.php",
        method: "GET",
        dataType: "text",
        success: function(data){
           console.log("cancellazione", data);

           var l;
           for(l=0;l<lista_json.length;l++) {
                var json=lista_json[l];
                json=vari_cari_di_rimuovi_caratteri_turtle(json);
                var pass = JSON.stringify(json);
                var url = url_doc_visual;
                
                var i=0;
                for (i=0; i<json.annotations.length; i++){

                    $.ajax({
                        url:"core/lib/annotation_manager/insert_graph.php",
                        method: "POST",
                        data: {mydata:pass, indice:i, u:url},
                        dataType: "text",
                        success: function(data){
                            //alert("Hai salvato le annotazioni da te create");
                            //console.log("Le annotazioni automatiche sono state salvate? "+data); //decommentare per vedere il turtle inviato
                            console.log("cancellazione Ho creato il turtle dell'annotazione in questione");
                        },
                        error: function(jqXHR,textStatus,textError){
                            console.log("Errore salvataggio server, funzione conversione_turtle: " +textStatus + " " + textError);
                        }
                    });

                    var json_steve={
                        annotations: [],
                        "provenance": {
                            "author": {
                                "name": "",
                                "email": ""
                            },
                            "time": ""
                        }
                    };

                    if(json.annotations[i].type=="cites") {
                        var p;
                        for(p=0;p<json.annotations[i].body.resource.annotations.length;p++) {
                            //citazione_per_stefano.push(lista_json[ultimo].annotations[k].body.resource.annotations[l]);
                            json_steve.annotations.push(json.annotations[i].body.resource.annotations[p]);
                        }
                        //citazione_per_stefano.push(lista_json[ultimo].annotations[k].body.resource);
                        //json_steve.annotations.push(citazione_per_stefano);
                        json_steve.provenance.author.name = json.provenance.author.name;
                        json_steve.provenance.author.email = json.provenance.author.email;
                        json_steve.provenance.time = json.provenance.time;
                    }
                    if(json_steve.annotations.length>0) {
                        var g;
                        for(g=0;g<json_steve.annotations.length;g++) {
                            json_steve.annotations[g].salvata=0;
                        }
                    }
                    conversione_turtle(json_steve);

                }

            }

        },
        error: function(){
            alert("cazzone " +textStatus + " " + textError);
        }
    });
}
