<?php
require_once "lib/scraper/dlibScraper.php";
require_once "lib/scraper/statisticaScraper.php";
require_once "lib/scraper/almatourismScraper.php";
require_once "lib/scraper/antropologiaeteatroScraper.php";
require_once "lib/scraper/genericScraper.php";


function parse_document($url) {
  try{
    if(parse_url($url) === false) {
      throw new exception("Invalid url to parse");
    }
    if(strpos($url, 'rivista-statistica.unibo.it') !== false) {
      $scraper = new statisticaScraper($url);
    }
    else if(strpos($url, 'dlib.org') !== false) {
      $scraper = new dlibScraper($url);
      }
      	else if(strpos($url, 'almatourism.unibo.it') !== false) {
      $scraper = new almatourismScraper($url);
    }
	else if(strpos($url, 'antropologiaeteatro.unibo.it') !== false) {
      $scraper = new antropologiaeteatroScraper($url);
    }
    else{
      // generic page, generic scraper
      $scraper = new genericScraper($url);
    }
    $scraper->parse();
    //header("Http/1.1 200 Ok");
    echo json_encode($scraper->data, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
  }
  catch(exception $e) {
    header("http/1.1 400 Bad request");
    $data = array();
    $data['error'] = 1;
    $data['errormsg'] = $e->getMessage();
    echo json_encode($data, JSON_UNESCAPED_SLASHES|JSON_UNESCAPED_UNICODE);
  }
}

header("content-type: application/json; charset=utf-8");
parse_document($_GET['url']);
?>
