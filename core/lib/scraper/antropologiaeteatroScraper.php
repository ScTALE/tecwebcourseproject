<?php
require_once "scraperBase.php";
require_once "almajournalsScraperBase.php";
require_once "scraperInterface.php";

/**
  * Scraper for Antropologia e Teatro, magazine available on almajournals
  */
  class antropologiaeteatroScraper extends almajournalsScraperBase implements scraperInterface{
      /**
  * Returns the magazine name
  */
  public function getMagazineName() {
    return 'antropologiaETeatro';
  }

  }

  ?>
