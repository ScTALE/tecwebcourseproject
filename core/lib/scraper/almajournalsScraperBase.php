<?php
/**
  * Base class for any almajournals magazine
  * Since every magazine on almajournals share the same structure the common logic is factored out in this base class.
*/

require_once "scraperBase.php";

abstract class almajournalsScraperBase extends scraperBase{
  var $url;

  public function __construct($article_url) {
    $this->loadDocument($article_url);
    $this->url = $article_url;
    $this->iri = $this->createDocumentIri($article_url);
      }

  public function parse() {
    $this->extractTitle();
    $this->extractDoi();
    $this->extractPublicationYear();
    $this->extractAuthor();
    $this->extractCitations();
    $this->extractUrl();
    $this->extractRhetoric();
    $this->setProvenance();
  }

  public function extractUrl() {
    $this->data['annotations'][] = array(
      'type' => 'hasUrl',
      'label' => 'Articolo disponibile all\'indirizzo',
      'body' => array(
        'subject' => $this->iri,
        'predicate' => 'fabio:hasUrl',
        'object' => $this->url,
      ),
      'target' => array(
        'source' => $this->iri,
        'id' => '',
        'start' => 0,
        'end' => 0,
        ),
    );
  }

  public function extractTitle() {
    $selector = "//div[@id='articleTitle']/h3";
    $nodes = $this->execXpath($selector);
    //it is safe to assume that only one node is returned and the cycle will be executed only once
    foreach($nodes as $node) {
      $title = trim(strip_tags($node->nodeValue));
      $this->data['annotations'][] = array(
        'type' => 'hasTitle',
        'label' => 'Il titolo del documento è ' . $title,
        'body' => array(
          'subject' => $this->iri,
          'predicate' => 'dcterms:title',
          'object' => $title,
        ),
        'target' => array(
          'source' => $this->iri,
          'id' => $this->encodeSelector($node->getNodePath()),
          'start' => 0,
          'end' => strlen($node->nodeValue),
        ),
      );
    }
  }

  public function extractPublicationYear() {
    $selector = "//meta[@name='DC.Date.issued']/@content";
    $nodes = $this->execXpath($selector);
    foreach($nodes as $node) {
      $year = date("Y", strtotime($node->nodeValue));
      $this->data['annotations'][] = array(
        'type' => 'hasPublicationYear',
        'label' => 'Anno di pubblicazione',
        'body' => array( //difhdsiofghdishfiosdhfiosdhfoihsdfiosdhfoidshfoih
          'subject' => $this->iri,
          'predicate' => 'fabio:hasPublicationYear',
          'object' => $year,
        ),
        'target' => array(
          'source' => $this->iri,
          'id' => '',
          'start' => 0,
          'end' => 0,
        ),
      );
    }
  }

  public function extractDoi() {
    $selector = "//meta[@name='DC.Identifier.DOI']/@content";
    $nodes = $this->execXpath($selector);
    foreach($nodes as $node) {
      $doi = $node->nodeValue;
      $this->data['annotations'][] = array(
        'type' => 'hasDOI',
        'label' => 'DOI',
        'body' => array(
          'subject' => $this->iri,
          'predicate' => 'prism:doi',
          'object' => $doi,
        ),
        'target' => array(
          'source' => $this->iri,
          'id' => '',
          'start' => 0,
          'end' => 0,
        ),
      );
    }
  }

  public function extractAuthor() {
    $selector = "//div[@id='authorString']/em";
    $nodes = $this->execXpath($selector);
    foreach($nodes as $node) {
      $authors = explode(', ', $node->nodeValue);
      $target_start = 0;
      for($i=0;$i<count($authors);$i++) {
        $target_end = $target_start+strlen($authors[$i]);
        $this->data['annotations'][] = array(
          'type' => 'hasAuthor',
          'label' => 'Autore',
          'body' => array(
            'label' => "Un autore del documento è $authors[$i].",
            'subject' => $this->iri,
            'predicate' => 'dcterms:creator',
            'resource' => array(
              'id' => $this->createPersonIri($authors[$i]),
              'label' => $authors[$i],
            ),
          ),
          'target' => array(
            'source' => $this->iri,
            'id' => $this->encodeSelector($node->getNodePath()),
            'start' => $target_start,
            'end' => $target_end,
          ),
        );
        $target_start = $target_end+2;
      }
    }
  }

  public function extractCitations() {
    $selector = "//div[@id='articleCitations']/div/p";
    $nodes = $this->execXpath($selector);
    $node_counter = 1;
    foreach($nodes as $node) {
        $cited_iri = $this->getMagazineName() . ':cited_' . $node_counter;
        $results = array();
      $regex = '/(.+)\(([0-9]{4})\)(.+)/';
      if(preg_match($regex, $node->nodeValue, $results, PREG_OFFSET_CAPTURE)) {
        //echo $node->nodeValue;
        //echo strlen($node->nodeValue);  
        $authors = explode(', ', $results[1][0]);
        $authors_start = $results[1][1];
        $pub_year = $results[2][0];
        $pub_year_start = $results[2][1];
        $title = $results[3][0];
        $title_start = $results[3][1];
        //prepares authors in advance, as they need special treatment
                $cited_authors = array();
                $target_end = strlen($node->nodeValue);
        for($i=0;$i<count($authors);$i++) {
          $cited_authors[] = array(
            'type' => 'hasAuthor',
            'label' => 'Autore',
            'body' => array(
              'label' => "Un autore del documento è $authors[$i].",
              'subject' => $cited_iri,
              'predicate' => 'dcterms:creator',
              'resource' => array(
                'id' => $this->createPersonIri($authors[$i]),
 'label' => $authors[$i],
              ),
            ),
            'target' => array(
              'source' => $this->iri,
              'id' => $this->encodeSelector($node->getNodePath()),
                          'start' => 0,
            'end' => strlen($node->nodeValue),
            ),
          );
          $authors_start = $target_end+2;
        }
        $this->data['annotations'][] = array(
          'type' => 'cites',
          'label' => 'Questo documento cita',
          'body' => array(
            'subject' => $this->iri,
            'predicate' => 'cito:cites',
            'resource' => array(
              'id' => $cited_iri,
              'label' => $node->nodeValue,
              // annotations on the cited document here
              'annotations' => array(
                //cited document title
                0 => array(
                  'type' => 'hasTitle',
                  'label' => 'Titolo',
                  'body' => array(
                    'subject' => $cited_iri,
                    'predicate' => 'dcterms:title',
                    'object' => $title,
                  ),
                  'target' => array(
                    'source' => $this->iri,
                    'id' => $this->encodeSelector($node->getNodePath()),
                                'start' => 0,
            'end' => strlen($node->nodeValue),
                  ),
                ),
                // cited document publication year
                1 => array(
                  'type' => 'hasPublicationYear',
                  'label' => 'Anno di pubblicazione',
                  'body' => array(
                    'subject' => $cited_iri,
                    'predicate' => 'fabio:hasPublicationYear',
                    'object' => $pub_year,
                  ),
                  'target' => array(
                    'id' => $this->encodeSelector($node->getNodePath()),
                    'source' => $this->iri,
                                'start' => 0,
            'end' => strlen($node->nodeValue),
                  ),
                ),
              ),
              //authors of cited documents will be added later
            ),
          ),
          'target' => array(
            'source' => $this->iri,
            'id' => $this->encodeSelector($node->getNodePath()),
            'start' => 0,
            'end' => strlen($node->nodeValue),
          ),
        );
                //adding cited document authors separately, so that they are not in a separate array
        $last_annotation_index = (count($this->data['annotations'])-1);
        foreach($cited_authors as $cited_author) {
          $this->data['annotations'][$last_annotation_index]['body']['resource']['annotations'][] = $cited_author;
        }
        $node_counter++;
      }
    }
  }

public function extractRhetoric() {
    //abstract
      $abstract_selector = "//div[@id='articleAbstract']/div/p";
    $nodes = $this->execXpath($abstract_selector);
    $abstract = $nodes->item(0)->nodeValue;
    // prevent errors in rare case where the abstract is under a different xpath
    if($abstract != NULL) {
          $this->data['annotations'][] = array(
      'label' => 'Abstract',
      'type' => 'denotesRhetoric',
      'body' => array(
        'label' => 'L\'abstract dell\'articolo è ' . $abstract,
        'subject' => $abstract,
        'predicate' => 'sem:denotes',
        'object' => 'sro:Abstract',
        ),
        'target' => array(
          'id' => $this->encodeSelector($node->getNodePath()),
          'start' => 0,
          'end' => strlen($abstract),
          ),
    );
    }
}

}

?>
