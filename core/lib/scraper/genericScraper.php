<?php
/**
* Scraper per pagine generiche
*/
require_once "scraperBase.php";
require_once "scraperInterface.php";

class genericScraper extends scraperBase implements scraperInterface{
  var $url;

  public function __construct($article_url) {
    $this->loadDocument($article_url);
    $this->url = $article_url;
    $this->iri = $this->createDocumentIri($article_url);
  }

  public function parse(){
    // only returns the URL annotation since we don't know what we're scraping
    $this->extractUrl();
  }

  public function getMagazineName(){
    return 'generic_page';
  }

  public function extractUrl() {
    $this->data['annotations'][] = array(
      'type' => 'hasUrl',
      'label' => 'Articolo disponibile a questo indirizzo',
      'body' => array(
        'subject' => $this->iri,
        'predicate' => 'fabio:hasUrl',
        'object' => $this->url,
      ),
      'target' => array(
        'source' => $this->iri,
        'id' => '',
        'start' => 0,
        'end' => 0,
        ),
    );
  }
}
?>
